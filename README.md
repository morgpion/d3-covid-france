
# D3 Covid France v1.0

## Description

Essai de d3 pour visualiser les nouveaux cas de Covid en France à partir de fichiers officiels.    

Il faut :    
*  trier les données pour traiter uniquement la France, 
*  calculer la différence entre jour[2] et jour[1] jusqu'à la fin des données pour france, 
*  et faire le graphique de ces données.

## Tech

**d3.js v5**





